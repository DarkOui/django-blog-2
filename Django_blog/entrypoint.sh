#!/bin/sh

sleep 5

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi
cd Django_blog
python manage.py flush --no-input
python manage.py migrate
gunicorn -b 0.0.0.0:8000 wsgi

exec "$@"
